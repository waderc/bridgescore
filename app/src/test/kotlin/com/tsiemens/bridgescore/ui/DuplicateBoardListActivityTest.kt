package com.tsiemens.bridgescore.ui

import com.tsiemens.bridgescore.Seat
import com.tsiemens.bridgescore.Seat.*
import com.tsiemens.bridgescore.orm.BridgeBoard
import com.tsiemens.bridgescore.orm.BridgeSession
import org.junit.Assert.assertEquals
import org.junit.Test


class DuplicateBoardListActivityTest {
    private fun newObj(lastEditBoard: Int,
                       boardNumList: List<Int>,
                       boardsPerRound: Int = 4,
                       boardsToDelete: List<Int> = emptyList()): DuplicateBoardListActivity {
        val act = DuplicateBoardListActivity()
        boardNumList.forEach { it ->
            val board = BridgeBoard(boardNumber = it)
            board.setid(it.toLong())
            act.boards.put(it.toLong(), board)
        }
        act.boardsToDelete.addAll(boardsToDelete.map({ it.toLong() }))
        act.session = BridgeSession(boardsPerRound = boardsPerRound, lastEditBoard = lastEditBoard)
        return act
    }

    @Test
    fun predictNextBoardNo() {
        val verify = { exp: Int?,
                       act: DuplicateBoardListActivity ->
            val session = act.session
            val boardNums = act.boards.map { it.value.boardNumber }
            val prediction = act.predictNextBoardNo()
            System.out.println(prediction.toString() +
                    " for boards " + boardNums.toString() +
                    ", to delete " + act.boardsToDelete.toString() +
                    ", last edit " + session!!.lastEditBoard.toString())
            assertEquals(exp, prediction)
        }

        // Initially, return null
        verify(null, newObj(0, listOf()))

        // The next highest unedited board number in the current round
        verify(2, newObj(1, listOf(1)))
        verify(3, newObj(2, listOf(1, 2)))
        verify(4, newObj(3, listOf(1, 2, 3)))

        // The round is complete, return null
        verify(null, newObj(4, listOf(1, 2, 3, 4)))

        // Fill in the gaps
        verify(1, newObj(1, listOf(2, 3, 4)))
        verify(2, newObj(1, listOf(1, 3, 4)))
        verify(3, newObj(1, listOf(1, 2, 4)))

        // Later round in the session, 3 boards per round
        verify(23, newObj(22, listOf(22), 3))

        // All boards in the round were deleted, don't care about last-edited in this case
        verify(null, newObj(22, listOf(), 3))

        // Deleted the last board in the round, we should re-enter it
        verify(4, newObj(4, listOf(1,2,3,4), boardsToDelete = listOf(4)))

        // Deleted two boards in the round, re-enter the lowest
        verify(3, newObj(4, listOf(1,2,3,4), boardsToDelete = listOf(3,4)))
    }

    private fun newSeatRound(lastEditBoard: Int,
                             boardInfoList: List<Pair<Int, Seat>>,
                             boardsPerRound: Int = 4,
                             boardsToDelete: List<Int> = emptyList()): DuplicateBoardListActivity {
        val act = DuplicateBoardListActivity()

        act.boards.putAll(boardInfoList.map { (idx, seat) ->
            val board = BridgeBoard(boardNumber = idx, mySeat = seat)
            board.setid(idx.toLong())
            Pair(idx.toLong(), board)
        })
        act.boardsToDelete.addAll(boardsToDelete.map({ it.toLong() }))
        act.session = BridgeSession(boardsPerRound = boardsPerRound,
                lastEditBoard = lastEditBoard,
                mySeatDefault = west)
        return act
    }

    @Test
    fun predictNextSeat() {
        val verify = { exp: Seat?,
                       act: DuplicateBoardListActivity ->
            val session = act.session
            val boardNums = act.boards.map {
                Pair(it.value.boardNumber,
                        it.value.mySeat)
            }
            val prediction = act.predictNextSeat()
            System.out.println(prediction.toString() +
                    " for boards " + boardNums.toString() +
                    ", to delete " + act.boardsToDelete.toString() +
                    ", last edit " + session!!.lastEditBoard.toString())
            assertEquals(exp, prediction)
        }

        // No last-edited board (no current round)
        verify(null, newSeatRound(0, listOf(Pair(1, north))))
        // Last edited, but deleted, board
        verify(null, newSeatRound(1, listOf()))
        // Inconsistent seat, do not predict
        verify(null, newSeatRound(3, listOf(
                Pair(1, north),
                Pair(2, north),
                Pair(3, south))))

        // Normal cases, consistent seat
        verify(north, newSeatRound(1, listOf(
                Pair(1, north))))
        verify(north, newSeatRound(2, listOf(
                Pair(1, north),
                Pair(2, north))))
        verify(north, newSeatRound(3, listOf(
                Pair(1, north),
                Pair(2, north),
                Pair(3, north))))
        verify(north, newSeatRound(3, listOf(
                Pair(1, north),
                Pair(2, north),
                Pair(3, north))))

        verify(east, newSeatRound(5, listOf(
                Pair(5, east))))

        // Finished round, no prediction for new round
        verify(null, newSeatRound(4, listOf(
                Pair(1, north),
                Pair(2, north),
                Pair(3, north),
                Pair(4, north))))

        // New round with boards for other rounds, get a prediction
        verify(east, newSeatRound(5, listOf(
                Pair(1, north),
                Pair(2, north),
                Pair(3, north),
                Pair(4, north),
                Pair(5, east)
                )))

        // Deleted board and boards for other rounds, get a prediction
        verify(east, newSeatRound(5, listOf(
                // round 1
                Pair(1, north),
                Pair(2, north),
                Pair(3, north),
                Pair(4, north),
                // round 2
                Pair(5, north), // deleted
                Pair(6, east),
                Pair(7, east),
                Pair(8, east)
                ),
                boardsToDelete = listOf(2, 3, 5)
        ))

        // Deleted board and boards for other rounds, get a prediction
        verify(north, newSeatRound(1, listOf(
                // round 1
                Pair(1, north),
                Pair(2, north),
                Pair(3, north),
                Pair(4, north),
                // round 2
                Pair(5, north), // deleted
                Pair(6, east),
                Pair(7, east),
                Pair(8, east)),
                boardsToDelete = listOf(2, 3, 5)
        ))
    }
}
