package com.tsiemens.bridgescore

import com.tsiemens.bridgescore.ui.prettyRoundedFloat
import org.junit.Test
import org.junit.Assert.*

class DisplayStringsTest {

    @Test
    fun prettyRoundedFloatTest() {
        assertEquals("100", prettyRoundedFloat(100.0f))
        assertEquals("99.9", prettyRoundedFloat(99.9f))
        assertEquals("50", prettyRoundedFloat(50.0f))
        assertEquals("33.3", prettyRoundedFloat(33.33333f))
        assertEquals("0", prettyRoundedFloat(0.0f))
    }

}