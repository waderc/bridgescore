package com.tsiemens.bridgescore

import com.tsiemens.bridgescore.orm.BridgeBoard
import org.junit.Test
import org.junit.Assert.*
import java.util.*


class BridgeScoringTest {

    val N = Seat.north
    val S = Seat.south
    val E = Seat.east
    val W = Seat.west

    val SP = Suit.spades
    val H = Suit.hearts
    val D = Suit.diamonds
    val C = Suit.clubs
    val NT = Suit.noTrumps

    val U = Doubled.undoubled
    val X = Doubled.doubled
    val XX = Doubled.redoubled

    fun board(boardNo: Int, level: Int, suit: Suit, doubled: Doubled, plusOrMinus: Int,
              declarer: Seat, mySeat: Seat, honors: Honors = Honors.none,
              nsHCP: Int = 0, fitLen: Int = 0, fitSuit: Suit = Suit.none,
              id: Long = 0L) : BridgeBoard {
        val board = BridgeBoard(boardNumber = boardNo,
                level = level,
                suit = suit,
                doubled = doubled,
                tricks = 6 + level + plusOrMinus,
                declarer = declarer,
                mySeat = mySeat,
                honors = honors,
                nsHighCardPoints = nsHCP,
                scoringLineFitLength = fitLen,
                scoringLineFitSuit = fitSuit,
                boardSetId = 1)
        board.id = id
        return board
    }

    @Test
    fun testDuplicateScore() {
        val boards = arrayOf(
                Pair( BridgeBoard(boardNumber = 1, level = 0, mySeat = N, boardSetId = 1), 0 ), // no bid

                Pair( board(1, 4, SP, U, 0, N, N), 420 ), // game
                Pair( board(2, 4, SP, U, 0, N, N), 620 ), // vul game
                Pair( board(1, 5, C, X, 0, N, N), 550 ), // game doubled
                Pair( board(1, 5, C, XX, 2, N, N), 1200 ), // game redoubled with overtricks
                Pair( board(4, 5, C, XX, 2, N, N), 1800 ), // game redoubled v with overtricks

                Pair( board(1, 1, D, U, 0, N, N), 70 ), // part score minor
                Pair( board(1, 1, H, U, 0, N, E), -80 ), // part score major
                Pair( board(1, 1, NT, U, 0, N, N), 90 ), // part score no trumps
                Pair( board(1, 1, D, U, -2, N, N), -100 ), // part score minor down
                Pair( board(1, 1, D, X, 0, N, N), 140 ), // part score minor doubled
                Pair( board(1, 3, D, X, 0, N, N), 470 ), // part score minor doubled into game
                Pair( board(1, 2, D, XX, 0, N, N), 560 ), // part score minor redoubled into game
                Pair( board(4, 1, D, XX, 3, N, N), 1430 ), // part score minor redoubled v

                Pair( board(1, 2, H, U, 2, N, S), 170 ), // part score major overtricks
                Pair( board(4, 2, H, U, 2, N, S), 170 ), // part score major overtricks v
                Pair( board(1, 2, H, X, 2, N, S), 670 ), // part score major overtricks x
                Pair( board(4, 2, H, X, 2, N, S), 1070 ), // part score major overtricks v x
                Pair( board(1, 2, H, XX, 2, N, E), -1040 ), // part score major overtricks xx
                Pair( board(4, 2, H, XX, 2, N, S), 1640 ), // part score major overtricks v xx

                Pair( board(1, 4, H, U, -1, E, N), 50 ), // -1
                Pair( board(1, 4, H, U, -4, E, W), -200 ), // -4
                Pair( board(4, 2, H, U, -1, E, W), -100 ), // -1 v
                Pair( board(1, 2, H, X, -1, E, W), -100 ), // -1 X
                Pair( board(4, 2, H, X, -2, E, W), -500 ), // -1 v X
                Pair( board(1, 2, H, XX, -6, E, W), -2800 ), // -1 XX
                Pair( board(4, 2, H, XX, -6, E, W), -3400 ), // -1 v XX

                Pair( board(3, 6, NT, U, 0, N, S), 990 ), // small slam
                Pair( board(3, 6, NT, U, 1, E, W), 1470 ), // small slam vul
                Pair( board(8, 7, D, U, 0, S, N), 1440 ), // vs grand slam
                Pair( board(4, 7, D, U, 0, N, E), -2140 ) // vs grand slam vul
        )

        for(b in boards) {
            System.out.println("Test Duplicate score for " + b.first.toString())
            assertTrue(b.first.isValid())
            assertEquals(b.second, duplicateBoardScore(b.first))
        }
    }

    @Test
    fun matchpointTest() {
        val boards = ArrayList<BridgeBoard>()
        boards.add( board(1, 4, SP, U, 0, N, N, id=1) )
        boards.add( board(1, 4, SP, U, 1, N, N, id=2) )
        boards.add( board(1, 4, SP, U, 2, N, N, id=3) )

        val matchpoints = boardMatchpoints(boards)
        assertEquals(matchpoints[1], 0.0f)
        assertEquals(matchpoints[2], 1.0f)
        assertEquals(matchpoints[3], 2.0f)
    }

    @Test
    fun matchpointTestEqual() {
        val boards = ArrayList<BridgeBoard>()
        boards.add( board(1, 4, SP, U, 0, N, N, id=1) )
        boards.add( board(1, 4, SP, U, 2, N, N, id=2) )
        boards.add( board(1, 4, SP, U, 1, N, N, id=3) )
        boards.add( board(1, 4, SP, U, 2, N, N, id=4) )
        boards.add( board(1, 4, SP, U, -1, N, N, id=5) )
        boards.add( board(1, 4, SP, U, 1, N, N, id=6) )

        val matchpoints = boardMatchpoints(boards)
        assertEquals(matchpoints[1], 1.0f)
        assertEquals(matchpoints[2], 4.5f)
        assertEquals(matchpoints[3], 2.5f)
        assertEquals(matchpoints[4], 4.5f)
        assertEquals(matchpoints[5], 0.0f)
        assertEquals(matchpoints[6], 2.5f)
    }

    @Test
    fun matchpointTestSingle() {
        val boards = ArrayList<BridgeBoard>()
        boards.add( board(1, 4, SP, U, 0, N, N, id=1) )

        val matchpoints = boardMatchpoints(boards)
        assertEquals(matchpoints[1], 0.0f)
    }

    @Test
    fun matchpointTestAllSame() {
        val boards = ArrayList<BridgeBoard>()
        boards.add( board(1, 4, SP, U, 0, N, N, id=1) )
        boards.add( board(1, 4, SP, U, 0, N, N, id=2) )
        boards.add( board(1, 4, SP, U, 0, N, N, id=3) )

        val matchpoints = boardMatchpoints(boards)
        assertEquals(matchpoints[1], 1.0f)
        assertEquals(matchpoints[2], 1.0f)
        assertEquals(matchpoints[3], 1.0f)
    }

    @Test
    fun testIMPScores() {
        assertEquals(0, boardIMP(0, 0))
        assertEquals(0, boardIMP(0, 10))
        assertEquals(-1, boardIMP(0, 20))
        assertEquals(1, boardIMP(30, 0))
        assertEquals(2, boardIMP(50, 0))
        assertEquals(2, boardIMP(60, 0))
        assertEquals(3, boardIMP(90, 0))
        assertEquals(3, boardIMP(100, 0))
        assertEquals(4, boardIMP(130, 0))
        assertEquals(4, boardIMP(140, 0))
        assertEquals(5, boardIMP(170, 0))
        assertEquals(5, boardIMP(180, 0))
        assertEquals(6, boardIMP(220, 0))
        assertEquals(6, boardIMP(230, 0))
        assertEquals(11, boardIMP(500, 0))
        assertEquals(11, boardIMP(510, 0))
        assertEquals(12, boardIMP(600, 0))
        assertEquals(12, boardIMP(740, 0))
        assertEquals(13, boardIMP(750, 0))
        assertEquals(13, boardIMP(751, 0))
        assertEquals(23, boardIMP(3500, 0))
        assertEquals(23, boardIMP(3990, 0))
        assertEquals(24, boardIMP(4000, 0))
        assertEquals(24, boardIMP(4100, 0))
    }

    class Rubber(val weAbove: Int, val weBelow: Int, val theyAbove: Int, val theyBelow: Int,
                 val boards: List<BridgeBoard>) {
        fun check() {
            val scores = rubberScoreboard(boards)
            var weAboveSum = 0
            var weBelowSum = 0
            var theyAboveSum = 0
            var theyBelowSum = 0
            for(below in scores.below) {
                if(below.we) {
                    weBelowSum += below.points
                } else {
                    theyBelowSum += below.points
                }
            }
            for(above in scores.weAbove) {
                weAboveSum += above
            }
            for(above in scores.theyAbove) {
                theyAboveSum += above
            }

            assertEquals(weAbove, weAboveSum)
            assertEquals(weBelow, weBelowSum)
            assertEquals(theyAbove, theyAboveSum)
            assertEquals(theyBelow, theyBelowSum)
        }
    }

    fun rubber(wa: Int, wb: Int, ta: Int, tb: Int, vararg boards: BridgeBoard): Rubber {
        return Rubber(wa, wb, ta, tb, boards.asList())
    }

    @Test
    fun rubberScoreTest() {
        rubber(0, 0, 0, 0).check()

        rubber(0, 140, 0, 130,
                board(1, 4, SP, U, 0, N, N),
                board(2, 3, SP, U, 0, W, N),
                board(3, 1, NT, U, 0, E, N),
                board(4, 1, C, U, 0, S, N)
                ).check()

        rubber(0, 0, 50, 0,
                board(1, 4, SP, U, -1, N, N)
        ).check()

        rubber(150, 120, 0, 0,
                board(1, 4, SP, U, 0, N, N, honors = Honors.all)
        ).check()

        rubber(0, 120, 150, 0,
                board(1, 4, SP, U, 0, N, N, honors = Honors.allDef)
        ).check()

        rubber(100, 120, 0, 0,
                board(1, 4, SP, U, 0, N, N, honors = Honors.partial)
        ).check()

        rubber(0, 120, 100, 0,
                board(1, 4, SP, U, 0, N, N, honors = Honors.partialDef)
        ).check()

        rubber(0, 210, 0, 0,
                board(1, 4, SP, U, 0, N, N),
                board(2, 3, SP, U, 0, N, N)
        ).check()

        // Make rubber with and without opponent opposition
        rubber(700, 320, 500, 260,
                board(1, 4, SP, U, 0, N, N),
                board(2, 5, D, U, 0, N, N),
                board(3, 4, D, U, 0, W, N),
                board(4, 4, D, U, 0, W, N),
                board(5, 3, NT, U, 0, S, N),
                board(6, 5, D, U, 0, W, N)
        ).check()

        // Rubber with partial opposition
        rubber(700, 220, 0, 60,
                board(1, 4, SP, U, 0, N, N),
                board(4, 3, D, U, 0, W, N),
                board(2, 5, D, U, 0, N, N)
        ).check()

        // Vuln and non-vuln undertricks
        rubber(100, 0, 50, 120,
                board(1, 4, SP, U, -1, N, N),
                board(4, 4, SP, U, 0, W, N),
                board(2, 5, D, U, -1, W, N)
        ).check()

        // Vuln doubled undertricks
        rubber(1100, 0, 0, 120,
                board(4, 4, SP, U, 0, W, N),
                board(2, 5, D, X, -4, W, N)
        ).check()

        // Vuln gone after rubber
        rubber(50, 0, 700, 240,
                board(4, 4, SP, U, 0, W, N),
                board(4, 4, SP, U, 0, W, N),
                board(2, 5, D, U, -1, W, N)
        ).check()
    }

    @Test
    fun parScoreTest() {
        val verify = { board: BridgeBoard, imps: Int ->
            System.out.println("Test par score IMPs for " + board.toString())
            assertTrue(board.isValid())
            assertEquals(imps, parScoreBoardIMPs(board))
        }

        // no bid
        verify( BridgeBoard(boardNumber = 1, level = 0, mySeat = N,
                nsHighCardPoints = 20, boardSetId = 1), -1 )
        verify( BridgeBoard(boardNumber = 4, level = 0, mySeat = N,
                nsHighCardPoints = 20, boardSetId = 1), 0 )
        verify( BridgeBoard(boardNumber = 1, level = 0, mySeat = N,
                nsHighCardPoints = 19, scoringLineFitLength = 0, boardSetId = 1), 1 )
        verify( BridgeBoard(boardNumber = 1, level = 0, mySeat = E,
                nsHighCardPoints = 20, boardSetId = 1), 1 )

        // Major 8 card fit (420 pts for this contract, 620 vuln)
        // --------------------------------------------
        // Made non-vuln
        verify( board(1, 4, SP, U, 0, N, N, nsHCP = 20, fitLen = 8, fitSuit = Suit.spades), 9 )
        verify( board(1, 4, SP, U, 0, N, N, nsHCP = 26, fitLen = 8, fitSuit = Suit.spades), 2 )
        // Down non-vuln
        verify( board(1, 4, SP, U, -1, N, N, nsHCP = 26, fitLen = 8, fitSuit = Suit.spades), -9 )
        // Made non-vuln will way too many pts
        verify( board(1, 4, SP, U, 0, N, N, nsHCP = 40, fitLen = 8, fitSuit = Suit.spades), -15 )
        // Made non-vuln by other other team
        verify( board(1, 4, SP, U, 0, E, N, nsHCP = 14, fitLen = 8, fitSuit = Suit.spades), -2 )
        verify( board(1, 4, SP, U, 0, E, N, nsHCP = 24, fitLen = 8, fitSuit = Suit.spades), -12 )
        verify( board(2, 4, SP, U, 0, E, N, nsHCP = 24, fitLen = 8, fitSuit = Suit.spades), -12 )
        // Made vuln
        verify( board(2, 4, SP, U, 0, N, N, nsHCP = 26, fitLen = 8, fitSuit = Suit.spades), 3 )
        verify( board(4, 4, SP, U, 0, N, N, nsHCP = 26, fitLen = 8, fitSuit = Suit.spades), 3 )
        verify( board(5, 4, SP, U, 0, N, N, nsHCP = 26, fitLen = 8, fitSuit = Suit.spades), 3 )
        verify( board(7, 4, SP, U, 0, N, N, nsHCP = 26, fitLen = 8, fitSuit = Suit.spades), 3 )
        verify( board(2, 4, SP, U, 0, N, N, nsHCP = 30, fitLen = 8, fitSuit = Suit.spades), -4 )
        // Made vuln by other team
        verify( board(3, 4, SP, U, 0, E, N, nsHCP = 14, fitLen = 8, fitSuit = Suit.spades), -3 )
        verify( board(4, 4, SP, U, 0, E, N, nsHCP = 14, fitLen = 8, fitSuit = Suit.spades), -3 )

        // Major 9 card fit
        // Non-vuln
        verify( board(1, 4, H, U, 0, N, N, nsHCP = 26, fitLen = 9, fitSuit = Suit.hearts), 0 )
        // Vuln
        verify( board(2, 4, H, U, 0, N, N, nsHCP = 26, fitLen = 9, fitSuit = Suit.hearts), 0 )

        // Major 10+ card fit
        // Non-vuln
        verify( board(1, 4, H, U, 0, N, N, nsHCP = 26, fitLen = 10, fitSuit = Suit.hearts), -1 )
        verify( board(1, 4, H, U, 0, N, N, nsHCP = 26, fitLen = 13, fitSuit = Suit.hearts), -1 )
        // Vuln
        verify( board(2, 4, H, U, 0, N, N, nsHCP = 26, fitLen = 10, fitSuit = Suit.hearts), -1 )
        verify( board(2, 4, H, U, 0, N, N, nsHCP = 26, fitLen = 13, fitSuit = Suit.hearts), -1 )


        // Minor 8 card fit (400 pts for contract, 600 vuln)
        // Non-vuln
        verify( board(1, 5, D, U, 0, N, N, nsHCP = 26, fitLen = 8, fitSuit = Suit.diamonds), 2 )
        // Vuln
        verify( board(2, 5, D, U, 0, N, N, nsHCP = 26, fitLen = 8, fitSuit = Suit.diamonds), 4 )

        // Minor 9 card fit
        // Non-vuln
        verify( board(1, 5, C, U, 0, N, N, nsHCP = 26, fitLen = 9, fitSuit = Suit.clubs), 2 )
        // Vuln
        verify( board(2, 5, C, U, 0, N, N, nsHCP = 26, fitLen = 9, fitSuit = Suit.clubs), 3 )

        // Minor 10+ card fit
        // Non-vuln
        verify( board(1, 5, C, U, 0, N, N, nsHCP = 26, fitLen = 10, fitSuit = Suit.clubs), -1 )
        verify( board(1, 5, C, U, 0, N, N, nsHCP = 26, fitLen = 13, fitSuit = Suit.clubs), -1 )
        // Vuln
        verify( board(2, 5, C, U, 0, N, N, nsHCP = 26, fitLen = 10, fitSuit = Suit.clubs), 1 )
        verify( board(2, 5, C, U, 0, N, N, nsHCP = 26, fitLen = 13, fitSuit = Suit.clubs), 1 )

        // No fit fit
        // Non-vuln
        verify( board(1, 4, H, U, 0, N, N, nsHCP = 26, fitLen = 0, fitSuit = Suit.none), 3 )
        // Vuln
        verify( board(2, 4, H, U, 0, N, N, nsHCP = 26, fitLen = 0, fitSuit = Suit.none), 4 )
    }
}
