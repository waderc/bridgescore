package com.tsiemens.bridgescore

import android.util.LongSparseArray
import android.util.SparseIntArray
import com.tsiemens.bridgescore.orm.BridgeBoard
import java.util.*

fun matchpointPct(mpts: Float, max: Int): Float {
    return if(max == 0) 50f else mpts / max * 100f
}

abstract class BoardPointsAggregator(val boardSets: Map<Long, Map<Int, BridgeBoard>>) {

    val boardNumbers = ArrayList<Int>()
    val teamPositions = LongSparseArray<Direction>()

    // Read-only properties

    /** Should return true of matchpoint methods are implemented and valid **/
    open fun matchpointsSupported(): Boolean = false

    /** Total matchpoints per team */
    open fun teamMatchpoints(boardSetId: Long): Float = throw UnsupportedOperationException()

    /** Matchpoints awarded for each board */
    open fun boardMatchpoints(boardId: Long): Float = throw UnsupportedOperationException()

    /** Maximum matchpoints possible for each board number, played from north/south */
    open fun maxMatchpointsForBoardNoNS(boardNo: Int): Int = throw UnsupportedOperationException()

    /** Maximum matchpoints possible for each board number, played from east/west */
    open fun maxMatchpointsForBoardNoEW(boardNo: Int): Int = throw UnsupportedOperationException()

    /** Maximum matchpoints possible for a team */
    open fun maxMatchpointsForTeam(boardSetId: Long): Int = throw UnsupportedOperationException()

    /** IMPs awarded for each board  */
    protected val _boardIMPs = LongSparseArray<Int>()
    fun boardIMPs(boardId: Long): Int? = _boardIMPs.get(boardId)

    /** Total IMPs awarded for each team */
    protected val _teamIMPs = LongSparseArray<Int>()
    fun teamIMPs(boardSetId: Long): Int? = _teamIMPs.get(boardSetId)

    /** Total normal duplcate/teams contract points awarded for each team */
    protected val _pairContractPoints = LongSparseArray<Int>()
    fun pairContractPoints(boardId: Long): Int = _pairContractPoints[boardId]

    open fun boardMatchpointPct(board: BridgeBoard): Float = throw UnsupportedOperationException()
    open fun teamMatchpointPct(boardSetId: Long): Float = throw UnsupportedOperationException()
    open fun maxBoardMatchpoints(board: BridgeBoard): Int = throw UnsupportedOperationException()

    private fun repopulateBoardNumbersAndTeamDirs() {
        val boardNumberSet = HashSet<Int>()
        for((setId, boards) in boardSets) {
            var nsCnt = 0
            var ewCnt = 0
            for((boardNo, board) in boards) {
                boardNumberSet.add(boardNo)
                if(board.playerIsNorthSouth()) {
                    nsCnt++
                } else {
                    ewCnt++
                }
            }
            // Take the average to get the team's orientation
            teamPositions.put(setId, if(nsCnt >= ewCnt) Direction.ns else Direction.ew)
        }
        boardNumbers.clear()
        boardNumbers.addAll(boardNumberSet)
        Collections.sort(boardNumbers)
    }

    open protected fun clearMatchpointsAndIMPs() {
        _boardIMPs.clear()

        _pairContractPoints.clear()
        _teamIMPs.clear()
    }

    protected abstract fun repopulateTeamMatchpointsAndIMPs()

    fun notifyBoardSetsChanged() {
        repopulateBoardNumbersAndTeamDirs()
        repopulateTeamMatchpointsAndIMPs()
    }
}

class DuplicateBoardAggregator(boardSets: Map<Long, Map<Int, BridgeBoard>>) :
        BoardPointsAggregator(boardSets) {

    override fun matchpointsSupported(): Boolean = true

    private val _teamMatchpoints = LongSparseArray<Float>()
    override fun teamMatchpoints(boardSetId: Long): Float = _teamMatchpoints[boardSetId]

    private val _boardMatchpoints = LongSparseArray<Float>()
    override fun boardMatchpoints(boardId: Long): Float = _boardMatchpoints[boardId]

    private val _maxMatchpointsForBoardNoNS = SparseIntArray()
    override fun maxMatchpointsForBoardNoNS(boardNo: Int): Int = _maxMatchpointsForBoardNoNS[boardNo]

    private val _maxMatchpointsForBoardNoEW = SparseIntArray()
    override fun maxMatchpointsForBoardNoEW(boardNo: Int): Int = _maxMatchpointsForBoardNoEW[boardNo]

    private val _maxMatchpointsForTeam = LongSparseArray<Int>()
    override fun maxMatchpointsForTeam(boardSetId: Long): Int = _maxMatchpointsForTeam[boardSetId]

    override fun boardMatchpointPct(board: BridgeBoard): Float {
        return matchpointPct(_boardMatchpoints[board.id!!],
                maxBoardMatchpoints(board))
    }

    override fun teamMatchpointPct(boardSetId: Long): Float {
        return matchpointPct(_teamMatchpoints[boardSetId],
                _maxMatchpointsForTeam[boardSetId])
    }

    override fun maxBoardMatchpoints(board: BridgeBoard): Int {
        return if(board.playerIsNorthSouth()) _maxMatchpointsForBoardNoNS[board.boardNumber]
        else _maxMatchpointsForBoardNoEW[board.boardNumber]
    }

    private fun updateBoardMatchpointsAndIMPs(boardNo: Int, boardsOfNumberForDirection: List<BridgeBoard>,
                                              maxMpsForBoardsInDirection: SparseIntArray) {
        // Matchpoints
        maxMpsForBoardsInDirection.put(boardNo, boardsOfNumberForDirection.size - 1)
        val mptsPerBoard = boardMatchpoints(boardsOfNumberForDirection)
        doAssert(mptsPerBoard.size == boardsOfNumberForDirection.size)
        for((bId, mp) in mptsPerBoard) {
            _boardMatchpoints.put(bId, mp)
        }

        // IMPs
        if(boardsOfNumberForDirection.size == 2) {
            val imp = boardIMP(duplicateBoardScore(boardsOfNumberForDirection[0]),
                    duplicateBoardScore(boardsOfNumberForDirection[1]))
            _boardIMPs.put(boardsOfNumberForDirection[0].id!!, imp)
            _boardIMPs.put(boardsOfNumberForDirection[1].id!!, imp * -1)
        }
    }

    override fun clearMatchpointsAndIMPs() {
        super.clearMatchpointsAndIMPs()
        _maxMatchpointsForBoardNoNS.clear()
        _maxMatchpointsForBoardNoEW.clear()
        _boardMatchpoints.clear()

        _teamMatchpoints.clear()
        _maxMatchpointsForTeam.clear()
    }

    override fun repopulateTeamMatchpointsAndIMPs() {
        clearMatchpointsAndIMPs()
        val boardsOfNumberNS = ArrayList<BridgeBoard>(boardNumbers.size)
        val boardsOfNumberEW = ArrayList<BridgeBoard>(boardNumbers.size)
        for(boardNo in boardNumbers) {
            for((id, boards) in boardSets) {
                val board = boards[boardNo]
                if(board != null) {
                    if(board.playerIsNorthSouth()) {
                        boardsOfNumberNS.add(board)
                    } else {
                        boardsOfNumberEW.add(board)
                    }
                }
            }
            updateBoardMatchpointsAndIMPs(boardNo, boardsOfNumberNS, _maxMatchpointsForBoardNoNS)
            updateBoardMatchpointsAndIMPs(boardNo, boardsOfNumberEW, _maxMatchpointsForBoardNoEW)

            boardsOfNumberNS.clear()
            boardsOfNumberEW.clear()
        }

        for((id, boards) in boardSets) {
            var teamMpts = 0f
            var maxTeamMpts = 0
            var teamDupPoints = 0
            var teamIMPTotal: Int? = null

            for(board in boards.values) {
                teamMpts += _boardMatchpoints[board.id!!]
                if(board.playerIsNorthSouth()) {
                    maxTeamMpts += _maxMatchpointsForBoardNoNS[board.boardNumber]
                } else {
                    maxTeamMpts += _maxMatchpointsForBoardNoEW[board.boardNumber]
                }

                teamDupPoints += duplicateBoardScore(board)

                // IMPs
                val boardIMP = _boardIMPs.get(board.id!!)
                if(boardIMP != null) {
                    teamIMPTotal = if(teamIMPTotal == null) 0 else teamIMPTotal
                    teamIMPTotal += boardIMP
                }
            }
            _teamMatchpoints.put(id, teamMpts)
            _maxMatchpointsForTeam.put(id, maxTeamMpts)
            _pairContractPoints.put(id, teamDupPoints)

            if(teamIMPTotal != null) {
                _teamIMPs.put(id, teamIMPTotal)
            }
        }
    }
}

class TeamsBoardAggregator(boardSets: Map<Long, Map<Int, BridgeBoard>>) :
        BoardPointsAggregator(boardSets) {

    var myBoardSetId: Long? = null
    var teammateBoardSetId: Long? = null

    private fun updateInferredIMPs(boardNo: Int, boardsOfNumberForDirection: List<BridgeBoard>) {
        if(boardsOfNumberForDirection.size == 2) {
            val usingSpecifiedTeam = (myBoardSetId != null && teammateBoardSetId != null)

            val imp = boardIMP(duplicateBoardScore(boardsOfNumberForDirection[0]),
                    duplicateBoardScore(boardsOfNumberForDirection[1]))

            // Set the inferred score for the given team if they are not us or we do not
            // have our other team's pair set
            if (!usingSpecifiedTeam || (
                    boardsOfNumberForDirection[0].boardSetId!! != myBoardSetId &&
                    boardsOfNumberForDirection[0].boardSetId!! != teammateBoardSetId)) {
                _boardIMPs.put(boardsOfNumberForDirection[0].id!!, imp)
            }
            if (!usingSpecifiedTeam || (
                    boardsOfNumberForDirection[1].boardSetId!! != myBoardSetId &&
                    boardsOfNumberForDirection[1].boardSetId!! != teammateBoardSetId)) {
                _boardIMPs.put(boardsOfNumberForDirection[1].id!!, imp * -1)
            }
        }
    }

    override fun repopulateTeamMatchpointsAndIMPs() {
        clearMatchpointsAndIMPs()
        val boardsOfNumberNS = ArrayList<BridgeBoard>(boardNumbers.size)
        val boardsOfNumberEW = ArrayList<BridgeBoard>(boardNumbers.size)
        for(boardNo in boardNumbers) {
            var myBoard: BridgeBoard? = null
            var teamBoard: BridgeBoard? = null
            for((id, boards) in boardSets) {
                val board = boards[boardNo]
                if(board != null) {
                    if(id == myBoardSetId) {
                        myBoard = board
                    } else if(id == teammateBoardSetId) {
                        teamBoard = board
                    }

                    if(board.playerIsNorthSouth()) {
                        boardsOfNumberNS.add(board)
                    } else {
                        boardsOfNumberEW.add(board)
                    }
                }
            }

            if(myBoard != null && teamBoard != null) {
                val imp = scoreToIMPs(duplicateBoardScore(myBoard) +
                                      duplicateBoardScore(teamBoard))
                _boardIMPs.put(myBoard.id!!, imp)
                _boardIMPs.put(teamBoard.id!!, imp)
            }

            updateInferredIMPs(boardNo, boardsOfNumberNS)
            updateInferredIMPs(boardNo, boardsOfNumberEW)

            boardsOfNumberNS.clear()
            boardsOfNumberEW.clear()
        }

        for((id, boards) in boardSets) {
            var pairContractPoints = 0
            var teamIMPTotal: Int? = null

            for(board in boards.values) {
                pairContractPoints += duplicateBoardScore(board)

                // IMPs
                val boardIMP = _boardIMPs.get(board.id!!)
                if(boardIMP != null) {
                    teamIMPTotal = if(teamIMPTotal == null) 0 else teamIMPTotal
                    teamIMPTotal += boardIMP
                }
            }
            _pairContractPoints.put(id, pairContractPoints)

            if(teamIMPTotal != null) {
                _teamIMPs.put(id, teamIMPTotal)
            }
        }
    }

}
