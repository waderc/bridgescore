package com.tsiemens.bridgescore

enum class ContractBridgeType {
    invalid,
    duplicate,
    teams,
    rubber,
    parScore
}

enum class Suit {
    none,
    clubs,
    diamonds,
    hearts,
    spades,
    noTrumps
}

enum class Doubled {
    undoubled,
    doubled,
    redoubled
}

enum class Seat {
    none,
    north,
    east,
    south,
    west;

    fun clockwiseDistFrom(seat: Seat): Int {
        doAssert(seat != none)
        return ( (seat.ordinal - north.ordinal) + 4 - (ordinal - north.ordinal) ) % 4
    }

    fun direction(): Direction? {
        return when(this) {
            north, south -> Direction.ns
            east, west -> Direction.ew
            none -> null
        }
    }
}

enum class Direction {
    ns,
    ew
}

enum class Vulnerability {
    none,
    ns,
    ew,
    both
}

enum class Honors(val strRes: Int) {
    none(R.string.honors_none),
    all(R.string.honors_all),
    partial(R.string.honors_part),
    allDef(R.string.honors_all_def),
    partialDef(R.string.honors_part_def)
}