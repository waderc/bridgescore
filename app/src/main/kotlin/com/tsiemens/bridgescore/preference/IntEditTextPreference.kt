package com.tsiemens.bridgescore.preference

import android.content.Context
import android.preference.EditTextPreference
import android.util.AttributeSet


class IntEditTextPreference : EditTextPreference {
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) :
        super(context, attrs, defStyleAttr, defStyleRes)

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) :
        super(context, attrs, defStyleAttr)

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context?) : super(context)

    override fun getPersistedString(defaultReturnValue: String?): String? {
        return getPersistedInt(-1).toString()
    }

    override fun persistString(value: String?): Boolean {
        try {
            return persistInt(value!!.toInt())
        } catch(e: NumberFormatException) {
            return false
        }
    }
}