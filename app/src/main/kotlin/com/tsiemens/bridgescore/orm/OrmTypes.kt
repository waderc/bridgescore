package com.tsiemens.bridgescore.orm

abstract class OrmRecord {
    // Awkward naming to avoid conflicts, because the id has to be declared in the child class
    // else SugarOrm gets confused
    // This is really only used when we are trying to abstract using OrmRecord
    abstract fun getid(): Long?
    abstract fun setid(v: Long?)

    abstract fun isValid(): Boolean
}

abstract class OrmWrapper {
    abstract fun <T: OrmRecord> findById(clazz: Class<T>, id: Long): T?
    abstract fun <T: OrmRecord> find(clazz: Class<T>, query: String, vararg args: String): List<T>
    abstract fun <T: OrmRecord> findAll(clazz: Class<T>): Iterator<T>
    abstract fun <T: OrmRecord> count(clazz: Class<T>, query: String, args: Array<String>): Long

    abstract fun save(obj: OrmRecord): Long
    abstract fun delete(obj: OrmRecord): Boolean

    abstract fun doInTransaction(callback: () -> Unit)

    fun deleteBoardSetAndBoards(set: BridgeBoardSet): Boolean {
        var retVal = false
        doInTransaction { ->
            for(board in set.boards()) {
                delete(board)
            }
            retVal = delete(set)
        }
        return retVal
    }

    fun deleteSessionAndBoards(session: BridgeSession): Boolean {
        var retVal = false
        doInTransaction { ->
            for(boardSet in session.boardSets()) {
                deleteBoardSetAndBoards(boardSet)
            }
            retVal = delete(session)
        }
        return retVal
    }
}

