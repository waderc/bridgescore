package com.tsiemens.bridgescore.orm

import com.orm.SugarRecord
import com.orm.SugarTransactionHelper
import com.tsiemens.bridgescore.doAssert

class SugarOrmWrapper : OrmWrapper() {
    override fun <T: OrmRecord> findById(clazz: Class<T>, id: Long): T? {
        return SugarRecord.findById(clazz, id)
    }

    override fun <T: OrmRecord> find(clazz: Class<T>, query: String, vararg args: String): List<T> {
        return SugarRecord.find(clazz, query, *args)
    }

    override fun <T: OrmRecord> findAll(clazz: Class<T>): Iterator<T> {
        return SugarRecord.findAll(clazz)
    }

    override fun <T: OrmRecord> count(clazz: Class<T>, query: String, args: Array<String>): Long {
        return SugarRecord.count<T>(clazz, query, args)
    }

    override fun save(obj: OrmRecord): Long {
        doAssert(obj.isValid())
        return SugarRecord.save(obj)
    }

    override fun delete(obj: OrmRecord): Boolean {
        return SugarRecord.delete(obj)
    }

    override fun doInTransaction(callback: () -> Unit) {
        SugarTransactionHelper.doInTransaction( callback )
    }

}