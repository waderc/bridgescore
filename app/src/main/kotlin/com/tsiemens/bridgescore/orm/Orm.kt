package com.tsiemens.bridgescore.orm

class Orm private constructor() {
    companion object {
        var ormWrapper: OrmWrapper = SugarOrmWrapper()

        fun <T: OrmRecord> findById(clazz: Class<T>, id: Long): T? {
            return ormWrapper.findById(clazz, id)
        }

        fun <T: OrmRecord> find(clazz: Class<T>, query: String, vararg args: String): List<T> {
            return ormWrapper.find(clazz, query, *args)
        }

        fun <T: OrmRecord> findAll(clazz: Class<T>): Iterator<T> {
            return ormWrapper.findAll(clazz)
        }

        fun <T: OrmRecord> count(clazz: Class<T>, query: String, args: Array<String>): Long {
            return ormWrapper.count(clazz, query, args)
        }

        fun save(obj: OrmRecord): Long = ormWrapper.save(obj)

        fun delete(obj: BridgeBoard): Boolean = ormWrapper.delete(obj)

        fun doInTransaction(callback: () -> Unit) = ormWrapper.doInTransaction( callback )

        fun deleteBoardSetAndBoards(set: BridgeBoardSet): Boolean =
                ormWrapper.deleteBoardSetAndBoards(set)

        fun deleteSessionAndBoards(session: BridgeSession): Boolean =
                ormWrapper.deleteSessionAndBoards(session)

    }
}


