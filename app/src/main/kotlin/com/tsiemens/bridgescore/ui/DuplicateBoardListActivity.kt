package com.tsiemens.bridgescore.ui

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.FragmentTransaction
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import com.tsiemens.bridgescore.*
import com.tsiemens.bridgescore.orm.BridgeBoard
import java.util.*

class DuplicateBoardListActivity : BoardListActivity() {
    companion object {
        private val SAVED_SPINNER_SELECTION = "round_spinner_idx"
    }

    var roundsList: ArrayList<String> = ArrayList(3)
    var roundsSpinnerAdapter: ArrayAdapter<String>? = null
    var roundsSpinner: Spinner? = null

    var updatingRoundsSpinner = false

    var isInitialSpinnerUpdate = true

    override fun contentLayoutId() = R.layout.activity_duplicate_board_list

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.setDisplayShowTitleEnabled(false)

        val listFrag = BoardListFragment()
        supportFragmentManager.beginTransaction().add(R.id.frag_container, listFrag)
                .setTransition(FragmentTransaction.TRANSIT_NONE)
                .commit()

        roundsSpinner = findViewById(R.id.spinner_round)
        roundsSpinnerAdapter = ArrayAdapter(this, R.layout.appbar_spinner_item, roundsList)
        roundsSpinnerAdapter!!.setDropDownViewResource(R.layout.simple_spinner_item)
        roundsSpinner!!.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if(!updatingRoundsSpinner) {
                    updateBoardList()
                }
            }
        }

        roundsSpinner!!.adapter = roundsSpinnerAdapter
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState!!.putInt(SAVED_SPINNER_SELECTION, roundsSpinner!!.selectedItemPosition)
    }

    override fun onResume() {
        super.onResume()
        updateRoundsSpinnerAndBoardList(isWaitingForBoardEditor)
        isWaitingForBoardEditor = false

        // Restore state cleared by orientation changes, etc.
        if(lastSavedInstanceState != null) {
            val spinnerSelection = lastSavedInstanceState!!.getInt(SAVED_SPINNER_SELECTION, -1)
            if(spinnerSelection != -1) {
                roundsSpinner!!.setSelection(spinnerSelection)
            }
            lastSavedInstanceState = null
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        menu!!.findItem(R.id.compare).isVisible = true
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId) {
            R.id.compare -> startDuplicateComparisonActivity()
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    private fun startDuplicateComparisonActivity() {
        val intent = Intent(this, DuplicateComparisonActivity::class.java)
        intent.putExtra(DuplicateComparisonActivity.BRIDGE_SESSION_ID, session!!.id)
        startActivity(intent)
    }

    private fun updateRoundsSpinner() {
        updatingRoundsSpinner = true
        val roundCnt = numberOfRounds()

        roundsList.clear()
        roundsList.add(getString(R.string.board_list_all_rounds))
        for(i in 1 .. roundCnt) {
            roundsList.add(fmtString(this, R.string.board_list_round_fmt, i ))
        }
        roundsSpinnerAdapter!!.notifyDataSetChanged()
        updatingRoundsSpinner = false
    }

    override fun updateAllViews() {
        updateRoundsSpinnerAndBoardList(false)
    }

    private fun getBoardRound(boardNum: Int) : Int {
        return ( ( boardNum - 1 ) / session!!.boardsPerRound ) + 1
    }

    private fun updateRoundsSpinnerAndBoardList(selectLast: Boolean) {
        updateRoundsSpinner()
        if(selectLast) {
            updatingRoundsSpinner = true
            val prevPos = roundsSpinner!!.selectedItemPosition
            val nextPos:Int
            if (session!!.lastEditBoard != 0) {
                nextPos = getBoardRound(session!!.lastEditBoard)
            } else {
                nextPos = roundsList.size - 1
            }
            roundsSpinner!!.setSelection(nextPos)
            if(!isInitialSpinnerUpdate && prevPos != nextPos && nextPos != 0) {
                // Changing this selection could be confusing.
                // Show a hint to try to clear this up.
                val fab: View = findViewById(R.id.fab)!!
                Snackbar.make(fab,
                        fmtString(this, R.string.board_round_change_hint_fmt, nextPos), 5000)
                        .setAction(R.string.board_round_change_hint_action, { v ->
                            roundsSpinner!!.setSelection(0)
                        })
                        .show()
            }
            updatingRoundsSpinner = false
        }
        isInitialSpinnerUpdate = false
        updateBoardList()
    }

    private fun numberOfRounds(): Int {
        val boardCnt = lastBoardNumber()
        val roundCnt: Int
        if(boardCnt == null) {
            roundCnt = 1
        } else {
            roundCnt = Math.ceil( boardCnt * 1.0 / session!!.boardsPerRound ).toInt()
        }
        return roundCnt
    }

    fun getCurrentFragment(): BaseBoardListFragment {
        return supportFragmentManager.findFragmentById(R.id.frag_container) as BaseBoardListFragment
    }

    fun updateBoardList() {
        val roundToShow = roundsSpinner!!.selectedItemPosition

        boardList.clear()
        if(roundToShow == 0) {
            boardList.addAll(nonDeletedBoards())
        } else {
            for(board in nonDeletedBoards()) {
                if(board.boardNumber > ( roundToShow - 1 ) * session!!.boardsPerRound &&
                        board.boardNumber <= roundToShow * session!!.boardsPerRound) {
                    boardList.add(board)
                }
            }
        }
        sortBoardList()
        getCurrentFragment().updateBoardsView()
    }

    override fun boardIsVulnerable(board: BridgeBoard): Boolean {
        return duplicateContractVulnerability(board)
    }

    override fun boardDisplayScore(board: BridgeBoard): Int? {
        return duplicateBoardScore(board)
    }

    override  fun getContractType(): ContractBridgeType = ContractBridgeType.duplicate

    /**
     * Handles deletions of boards from fragments to notify that the boardsList has been modified
     * */
    override fun notifyBoardsListChanged() {
        if(roundsSpinner!!.selectedItemPosition == 0) {
            updateRoundsSpinner()
        } else if(boardList.isEmpty()) {
            updateRoundsSpinnerAndBoardList(true)
        } else {
            getCurrentFragment().updateBoardsView()
        }
    }

    private fun getRoundInfo(): RoundInfoHelper {
        val currentRound: Int? =
                if (session!!.lastEditBoard == 0) null
                else getBoardRound(session!!.lastEditBoard)
        return RoundInfoHelper(currentRound, session!!.boardsPerRound, nonDeletedBoards())
    }

    /**
     * Get the expected next board number to be created.
     * This will first guess a board in the last-edited round where it is the only missing board
     * for the round.
     * Next attempt will be the last-edited board + 1, if that falls in the current round.
     * Otherwise, null is returned.
     * eg. If the last round is 2 (3 bps) and we have played boards 4 and 6, return 5
     * eg. If the last board entered (3 bps) was 22, return 23.  If 22, 23, and 24 was most recently
     *     entered, then return null (new round).
     */
    override fun predictNextBoardNo(): Int? {
        val info = getRoundInfo()
        if (info.virgin()) {
            return null
        } else {
            return info.boardsLeft.min()
        }
    }

    /**
     * Get the expected next seat position to use.
     * For duplicate, if a round is active, use the majority seat position for
     * boards that have already been entered.
     *
     * In the case of a Howell movement[1], the seat of a player will vary per-round,
     * but stay consistent within a given round.
     *
     * [1] https://en.wikipedia.org/wiki/Duplicate_bridge_movements#Howell_movements
     */

    override fun predictNextSeat(): Seat? {
        val info = getRoundInfo()
        if (info.virgin()) {
            return null
        }
        var prediction: Seat? = null
        for(board in info.activeBoardList.filter { it.boardNumber in info.boardsInRound }) {
            if (prediction == null) {
                prediction = board.mySeat
            } else if (prediction != board.mySeat) {
                return null
            } else {
                // nothing to do
            }
        }
        return prediction
    }
}

class RoundInfoHelper(currentRound: Int?,
                      boardsPerRound: Int,
                      nonDeletedBoards : List<BridgeBoard>) {
    val boardsInRound = HashSet<Int>()
    val boardsLeft = HashSet<Int>()
    val activeBoardList = nonDeletedBoards

    init {
        if (currentRound != null) {
            val firstInRound = ((currentRound - 1) * boardsPerRound) + 1
            val lastInRound = currentRound * boardsPerRound

            for (i in firstInRound..lastInRound) {
                boardsLeft.add(i)
            }

            for (board in nonDeletedBoards) {
                if (board.boardNumber >= firstInRound && board.boardNumber <= lastInRound) {
                    boardsInRound.add(board.boardNumber)
                    boardsLeft.remove(board.boardNumber)
                }
            }
        }
    }

    /**
     * virgin: true when there are either no boards in the round currently,
     * or if all of the boards in the round have already been entered.
     */
    fun virgin(): Boolean {
        return boardsInRound.isEmpty() or boardsLeft.isEmpty()
    }
}
