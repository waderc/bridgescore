package com.tsiemens.bridgescore.ui


import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.preference.Preference
import android.preference.PreferenceFragment
import android.preference.PreferenceManager
import android.view.MenuItem
import android.widget.Toast

import com.tsiemens.bridgescore.R
import com.tsiemens.bridgescore.preference.*

/**
 * A [PreferenceActivity] that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 *
 *
 * See [
   * Android Design: Settings](http://developer.android.com/design/patterns/settings.html) for design guidelines and the [Settings
   * API Guide](http://developer.android.com/guide/topics/ui/settings.html) for more information on developing a Settings UI.
 */
class SettingsActivity : AppCompatPreferenceActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar.setDisplayHomeAsUpEnabled(true)

        // Display the fragment as the main content.
        fragmentManager.beginTransaction()
                .replace(android.R.id.content, GeneralPreferenceFragment())
                .commit();
    }

    /**
     * {@inheritDoc}
     */
    override fun onIsMultiPane(): Boolean {
        return isXLargeTablet(this)
    }

    /**
     * This method stops fragment injection in malicious applications.
     * Make sure to deny any unknown fragments here.
     */
    override fun isValidFragment(fragmentName: String): Boolean {
        return PreferenceFragment::class.java.name == fragmentName
                || GeneralPreferenceFragment::class.java.name == fragmentName
    }

    /**
     * This fragment shows general preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    class GeneralPreferenceFragment : PreferenceFragment() {
        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            addPreferencesFromResource(R.xml.pref_general)
            setHasOptionsMenu(true)

            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
            // to their values. When their values change, their summaries are
            // updated to reflect the new value, per the Android Design
            // guidelines.
            bindPreferenceSummaryToValue(findPreference(PREF_DEFAULT_SESSION_NAME))
            bindPreferenceSummaryToValue(findPreference(PREF_DEFAULT_TEAM_NAME))
            bindPreferenceSummaryToValue(findPreference(PREF_DEFAULT_BOARDS_PER_ROUND))
            bindPreferenceSummaryToValue(findPreference(PREF_DEFAULT_SYNC_SERVER))
            bindPreferenceSummaryToValue(findPreference(PREF_DEFAULT_SYNC_SERVER_PASSWORD))
        }

        override fun onOptionsItemSelected(item: MenuItem): Boolean {
            val id = item.itemId
            if (id == android.R.id.home) {
                activity.finish()
                return true
            }
            return super.onOptionsItemSelected(item)
        }
    }

    companion object {
        /**
         * A preference value change listener that updates the preference's summary
         * to reflect its new value.
         */
        private val bindPreferenceSummaryToValueListener =
                Preference.OnPreferenceChangeListener { preference, value ->
            if(preference is IntEditTextPreference) {
                val intVal: Int
                try {
                    intVal = value.toString().toInt()
                    when(preference.key) {
                        PREF_DEFAULT_BOARDS_PER_ROUND ->
                                if(intVal <= 0) {
                                    Toast.makeText(preference.context,
                                            R.string.default_boards_value_warning,
                                            Toast.LENGTH_LONG).show()
                                    return@OnPreferenceChangeListener false
                                }
                        else -> throw IllegalArgumentException(preference.key)
                    }

                } catch(e: NumberFormatException) {
                    Toast.makeText(preference.context, R.string.int_pref_value_warning,
                            Toast.LENGTH_SHORT).show()
                    return@OnPreferenceChangeListener false
                }
            }
            if(preference.key == PREF_DEFAULT_SYNC_SERVER_PASSWORD) {
                preference!!.summary = if( value.toString().isEmpty() ) "" else "••••••"
            } else {
                preference!!.summary = value.toString()
            }
            true
        }

        /**
         * Helper method to determine if the device has an extra-large screen. For
         * example, 10" tablets are extra-large.
         */
        private fun isXLargeTablet(context: Context): Boolean {
            return (context.resources.configuration.screenLayout
                    and Configuration.SCREENLAYOUT_SIZE_MASK) >=
                    Configuration.SCREENLAYOUT_SIZE_XLARGE
        }

        /**
         * Binds a preference's summary to its value. More specifically, when the
         * preference's value is changed, its summary (line of text below the
         * preference title) is updated to reflect the value. The summary is also
         * immediately updated upon calling this method. The exact display format is
         * dependent on the type of preference.

         * @see .sBindPreferenceSummaryToValueListener
         */
        private fun bindPreferenceSummaryToValue(preference: Preference) {
            // Set the listener to watch for value changes.
            preference.onPreferenceChangeListener = bindPreferenceSummaryToValueListener

            // Trigger the listener immediately with the preference's
            // current value.
            val newVal: Any
            if(preference is IntEditTextPreference) {
                newVal = PreferenceManager.getDefaultSharedPreferences(preference.context).getInt(preference.key, -1)
            } else {
                newVal = PreferenceManager.getDefaultSharedPreferences(preference.context).getString(preference.key, "")
            }

            bindPreferenceSummaryToValueListener.onPreferenceChange(preference, newVal)
        }
    }
}
