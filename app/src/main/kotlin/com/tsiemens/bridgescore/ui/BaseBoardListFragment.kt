package com.tsiemens.bridgescore.ui

import android.support.v4.app.Fragment
import com.tsiemens.bridgescore.orm.BridgeBoard
import java.util.*

abstract class BaseBoardListFragment : Fragment() {
    val brdActvity: BoardListActivity
        get() = (activity!! as BoardListActivity)

    val boardList: ArrayList<BridgeBoard>
        get() = brdActvity.boardList

    abstract fun updateBoardsView()
}