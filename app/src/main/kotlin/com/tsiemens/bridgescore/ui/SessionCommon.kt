package com.tsiemens.bridgescore.ui

import android.app.Dialog
import android.app.DialogFragment
import android.content.DialogInterface
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AlertDialog
import android.view.View
import android.widget.CheckBox
import android.widget.EditText
import android.widget.RadioGroup
import android.widget.Toast
import com.tsiemens.bridgescore.orm.BridgeSession
import com.tsiemens.bridgescore.ContractBridgeType
import com.tsiemens.bridgescore.R
import com.tsiemens.bridgescore.Seat
import com.tsiemens.bridgescore.api.ApiSession
import com.tsiemens.bridgescore.doAssert
import com.tsiemens.bridgescore.orm.BridgeBoard
import com.tsiemens.bridgescore.orm.BridgeBoardSet
import com.tsiemens.bridgescore.preference.PREF_DEFAULT_BOARDS_PER_ROUND
import com.tsiemens.bridgescore.preference.PREF_DEFAULT_SESSION_NAME

class EditSessionDialogFragment(val session: BridgeSession? = null,
                                val myBoardSet: BridgeBoardSet? = null) : DialogFragment() {

    var onSaveButtonAction: ((EditSessionDialogFragment)-> Boolean)? = null

    var mySeatHelper: SeatSelectionHelper? = null
    var mySeatUpdateCheckbox: CheckBox? = null
    var isFirstSeatChange = true
    var contentView: View? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog? {
        val builder = AlertDialog.Builder(activity);
        contentView = activity.layoutInflater.inflate(R.layout.session_dialog, null)

        contentView!!.findViewById<RadioGroup>(R.id.rg_session_type)
                .setOnCheckedChangeListener { radioGroup, i ->
                    contentView!!.findViewById<View>(R.id.ll_boards_per_round).visibility =
                            when (getSelectedBridgeType()) {
                                ContractBridgeType.duplicate,
                                ContractBridgeType.teams ->
                                    View.VISIBLE
                                ContractBridgeType.rubber,
                                ContractBridgeType.parScore ->
                                    View.GONE
                                ContractBridgeType.invalid -> throw IllegalStateException()
                            }
                }

        val seatRg: RadioGroup = contentView!!.findViewById(R.id.rg_my_seat)
        mySeatHelper = SeatSelectionHelper(seatRg,
                R.id.rb_north, R.id.rb_east, R.id.rb_south, R.id.rb_west)
        mySeatUpdateCheckbox = contentView!!.findViewById(R.id.checkbox_update_my_seat)

        seatRg.setOnCheckedChangeListener { radioGroup, i ->
            if(session != null && session.mySeatDefault != mySeatHelper!!.getSelectedSeat()) {
                if(isFirstSeatChange) {
                    mySeatUpdateCheckbox!!.isChecked = true
                }
                isFirstSeatChange = false
            }
        }

        doAssert((session != null) == (myBoardSet != null))
        populateFields(contentView!!)

        builder.setMessage(fmtString(activity, R.string.session_edit_title_fmt,
                if(session != null) getString(R.string.edit) else getString(R.string.neww)))
                .setPositiveButton(android.R.string.ok, null)
                .setNegativeButton(android.R.string.cancel, null)
                .setView(contentView)

        return builder.create()
    }

    override fun onStart() {
        super.onStart()
        // Override the default button behaviour so we can prevent it from always dismissing
        val button = (dialog as AlertDialog).getButton(DialogInterface.BUTTON_POSITIVE)
        button.setOnClickListener { v -> onPositiveButton() }
    }

    fun populateFields(view: View) {
        val bprEt: EditText = view.findViewById(R.id.et_boards_per_round)
        bprEt.text.clear()
        val typeRg: RadioGroup = view.findViewById(R.id.rg_session_type)
        if(session == null) {
            val prefs = PreferenceManager.getDefaultSharedPreferences(activity)

            val defaultBpr = prefs.getInt(PREF_DEFAULT_BOARDS_PER_ROUND,
                            activity.resources.getInteger(R.integer.default_boards_per_round))
            bprEt.text.append(defaultBpr.toString())

            mySeatUpdateCheckbox!!.visibility = View.GONE
        } else {
            // Disallow changing session type
            typeRg.visibility = View.GONE
            typeRg.check(when(session.type) {
                ContractBridgeType.duplicate -> R.id.rb_duplicate
                ContractBridgeType.teams -> R.id.rb_teams
                ContractBridgeType.rubber -> R.id.rb_rubber
                ContractBridgeType.parScore -> R.id.rb_par_score
                else -> throw IllegalStateException()
            })
            when (session.type) {
                ContractBridgeType.duplicate,
                ContractBridgeType.teams ->
                    bprEt.text.append(session.boardsPerRound.toString())
                ContractBridgeType.rubber,
                ContractBridgeType.parScore ->
                    view.findViewById<View>(R.id.ll_boards_per_round).visibility = View.GONE
                ContractBridgeType.invalid -> throw IllegalStateException()
            }
            mySeatHelper!!.setSelectedSeat(session.mySeatDefault)
        }
    }

    fun getSelectedBridgeType(): ContractBridgeType {
        return when((contentView!!.findViewById<RadioGroup>(R.id.rg_session_type))
                .checkedRadioButtonId) {
            R.id.rb_duplicate -> ContractBridgeType.duplicate
            R.id.rb_teams -> ContractBridgeType.teams
            R.id.rb_rubber -> ContractBridgeType.rubber
            R.id.rb_par_score -> ContractBridgeType.parScore
            else -> throw IllegalStateException()
        }
    }

    fun onPositiveButton() {
        if(onSaveButtonAction != null) {
            if(onSaveButtonAction!!(this)) {
                dialog.dismiss()
            }
        } else {
            dialog.dismiss()
        }
    }

    fun writeToSession(session: BridgeSession, boards: Iterable<BridgeBoard>? = null): Boolean {
        val type = getSelectedBridgeType()

        var boardsPerRound: Int = 0
        when(type) {
            ContractBridgeType.duplicate,
            ContractBridgeType.teams -> {
                try {
                    val bprText = contentView!!.findViewById<EditText>(R.id.et_boards_per_round)
                            .text.toString()
                    if (bprText.isEmpty()) {
                        Toast.makeText(activity, R.string.session_editor_no_bpr,
                                Toast.LENGTH_SHORT).show()
                        return false
                    }
                    boardsPerRound = bprText.toInt()
                } catch(e: NumberFormatException) {}

                if (boardsPerRound == 0) {
                    Toast.makeText(activity, R.string.session_editor_bpr_error,
                            Toast.LENGTH_SHORT).show()
                    return false
                }
            }
            ContractBridgeType.parScore,
            ContractBridgeType.rubber ->
                boardsPerRound = 1
            ContractBridgeType.invalid ->
                throw IllegalStateException()
        }

        val mySeat = mySeatHelper!!.getSelectedSeat()
        if(mySeat == Seat.none) {
            Toast.makeText(activity, R.string.editor_no_my_seat, Toast.LENGTH_SHORT).show()
            return false
        }

        session.type = type
        session.boardsPerRound = boardsPerRound
        session.mySeatDefault = mySeat

        if(this.session != null && mySeatUpdateCheckbox!!.isChecked) {
            for(board in boards!!) {
                board.mySeat = mySeat
            }
        }
        return true
    }
}

class NewRemoteSessionDialogFragment(val session: BridgeSession? = null) : DialogFragment() {
    var contentView: View? = null
    var onDoneAction: ((ApiSession, Boolean) -> Unit)? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog? {
        val builder = AlertDialog.Builder(activity);
        contentView = activity.layoutInflater.inflate(R.layout.remote_session_dialog, null)

        doAssert(session != null)
        populateFields(contentView!!)

        builder.setMessage(fmtString(activity, R.string.new_remote_session_title_fmt,
                session!!.syncServer))
                .setPositiveButton(android.R.string.ok, null)
                .setNegativeButton(android.R.string.cancel, null)
                .setView(contentView)

        return builder.create()
    }

    fun populateFields(view: View) {
        val prefs = PreferenceManager.getDefaultSharedPreferences(activity)
        val sessNameText: EditText = view.findViewById(R.id.et_sess_name)
        sessNameText.text.clear()
        sessNameText.text.append(prefs.getString(PREF_DEFAULT_SESSION_NAME, ""))
    }

    override fun onStart() {
        super.onStart()
        // Override the default button behaviour so we can prevent it from always dismissing
        val button = (dialog as AlertDialog).getButton(DialogInterface.BUTTON_POSITIVE)
        button.setOnClickListener { v -> onPositiveButton() }
    }

    fun onPositiveButton() {
        val sessName = contentView!!.findViewById<EditText>(R.id.et_sess_name).text.toString()
        if(sessName.isEmpty()) {
            Toast.makeText(activity, R.string.session_name_required, Toast.LENGTH_SHORT).show()
            return
        }
        val saveAsDefault = contentView!!.findViewById<CheckBox>(R.id.checkbox_session_defaults)
            .isChecked
        val sess = ApiSession(0, sessName, session!!.timestamp)
        onDoneAction!!(sess, saveAsDefault)
        dialog.dismiss()
    }
}

