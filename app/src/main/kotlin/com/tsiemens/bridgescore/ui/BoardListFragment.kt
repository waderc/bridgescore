package com.tsiemens.bridgescore.ui

import android.os.Bundle
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.view.ActionMode
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.ImageView
import com.tsiemens.bridgescore.*
import com.tsiemens.bridgescore.orm.BridgeBoard
import java.util.*

class BoardListFragment : BaseBoardListFragment() {
    val selectedBoards = HashSet<Long>()

    var mAdapter: BoardListAdapter? = null
    val adapter: BoardListAdapter
        get() = mAdapter!!

    var actionMode: ActionMode? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mAdapter = BoardListAdapter(brdActvity.boardList, selectedBoards)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val v = inflater!!.inflate(R.layout.fragment_board_list, container, false)

        val recycler = v.findViewById<RecyclerView>(R.id.list)
        recycler.setHasFixedSize(true)
        val layoutMgr = LinearLayoutManager(brdActvity)
        recycler.layoutManager = layoutMgr

        adapter.circleDataViewId = R.id.tv_board_number
        adapter.onItemClick = { v, i -> brdActvity.startBoardEditor(boardList[i].id) }
        adapter.onItemSelectorClick = { v, i ->
            if(selectedBoards.contains(boardList[i].id)) {
                selectedBoards.remove(boardList[i].id)
                if(selectedBoards.isEmpty()) {
                    actionMode!!.finish()
                }
            } else {
                selectedBoards.add(boardList[i].id!!)
                if(actionMode == null) {
                    actionMode = brdActvity.startSupportActionMode(BoardsActionModeCb())
                }
            }
        }
        recycler.adapter = adapter

        val params = brdActvity.findViewById<View>(R.id.fab)!!.layoutParams as CoordinatorLayout.LayoutParams
        params.anchorId = R.id.list
        brdActvity.findViewById<View>(R.id.fab)!!.layoutParams = params

        return v
    }

    override fun onResume() {
        super.onResume()
        updateBoardsView()
    }

    override fun updateBoardsView() {
        adapter.notifyDataSetChanged()
    }

    inner class BoardsActionModeCb() : ActionMode.Callback {
        override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {
            MenuInflater(brdActvity).inflate(R.menu.delete_action_menu, menu)
            return true
        }

        override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
            return false
        }

        override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {
            if(item!!.itemId == R.id.delete) {
                // Clear previous delete queue
                brdActvity.processPendingDeletes(true)

                for(board in boardList) {
                    if(selectedBoards.contains(board.id)) {
                        brdActvity.boardsToDelete.add(board.id!!)
                    }
                }

                boardList.removeAll { board -> brdActvity.boardsToDelete.contains(board.id) }
                brdActvity.notifyBoardsListChanged()

                val fab: View = brdActvity.findViewById(R.id.fab)!!
                Snackbar.make(fab, fmtString(brdActvity, R.string.board_list_deleted_boards_fmt,
                        brdActvity.boardsToDelete.size), 5000)
                        .setCallback(object: Snackbar.Callback() {
                            override fun onDismissed(snackbar: Snackbar?, event: Int) {
                                (activity as BoardListActivity?)?.processPendingDeletes(event !=
                                        Snackbar.Callback.DISMISS_EVENT_ACTION)
                            }
                        })
                        .setAction(R.string.undo, { v -> }).show()
                actionMode!!.finish()
                return true
            }
            return false
        }

        override fun onDestroyActionMode(mode: ActionMode?) {
            actionMode = null
            adapter.notifyDataSetChanged()
            selectedBoards.clear()
        }
    }

    inner class BoardListAdapter(list: List<BridgeBoard>, selections: Set<Long>) :
            CircleCheckListAdapter<BridgeBoard>(list, selections, R.layout.board_list_item) {

        override fun onBindViewHolder(holder: CircleCheckViewHolder<BridgeBoard>?, position: Int) {
            val board = list[position]
            val bufferView: View = holder!!.itemView.findViewById(R.id.bottom_buffer_view)
            bufferView.visibility = if(position == list.size - 1) View.VISIBLE else View.GONE

            val boardNumTv = getTextView(holder, R.id.tv_board_number)
            boardNumTv.text = board.boardNumber.toString()

            // Display the notes indicator
            holder.itemView.findViewById<View>(R.id.rl_notes_indicator).visibility =
                    if(board.notes != null) View.VISIBLE else View.GONE

            val myPosTv = getTextView(holder, R.id.tv_declarer)
            if(board.level == 0) {
                myPosTv.text = ""
            } else {
                myPosTv.text = fmtString(activity, R.string.declarer_fmt,
                        getString(when(board.declarer) {
                            Seat.north -> R.string.north
                            Seat.east -> R.string.east
                            Seat.south -> R.string.south
                            Seat.west -> R.string.west
                            Seat.none -> throw IllegalStateException(
                                    "Unexpected declarer:" + board.declarer)
                        }))
            }

            // This will center the "No Bid" view
            holder.itemView.findViewById<View>(R.id.ll_tricks_and_position).visibility =
                if(board.level == 0) View.GONE else View.VISIBLE

            val isParScoreMode = brdActvity.session!!.type == ContractBridgeType.parScore
            holder.itemView.findViewById<View>(R.id.ll_par_score).visibility =
                    if(isParScoreMode) View.VISIBLE else View.GONE
            holder.itemView.findViewById<View>(R.id.tv_score).visibility =
                    if(isParScoreMode) View.GONE else View.VISIBLE
            holder.itemView.findViewById<View>(R.id.ll_scoring_line).visibility =
                    if(isParScoreMode) View.VISIBLE else View.GONE
            if(isParScoreMode) {
                val imps = parScoreBoardIMPs(board)
                val impsTv = getTextView(holder, R.id.tv_imps)
                impsTv.text = Math.abs(imps).toString()
                impsTv.setTextColor(getPointsDisplayColor(context, imps))

                val scoreTv = getTextView(holder, R.id.tv_score_sub)
                setBoardPointsText(scoreTv, brdActvity.boardDisplayScore(board)!!, "(%s)")

                val slPair = scoringLinePair(board.boardNumber, board.nsHighCardPoints)!!
                val slPairTv = getTextView(holder, R.id.tv_scoring_line_pair)
                slPairTv.setTextColor(ContextCompat.getColor(activity, when(slPair) {
                    Direction.ns -> R.color.colorNS
                    Direction.ew -> R.color.colorEW
                }))
                slPairTv.text = getString(when(slPair) {
                    Direction.ns -> R.string.north_south
                    Direction.ew -> R.string.east_west
                })

                val slHcpTv = getTextView(holder, R.id.tv_scoring_line_hcp)
                slHcpTv.text = fmtString(activity, R.string.hcp_fmt,
                        if(slPair == Direction.ns) board.nsHighCardPoints
                        else 40 - board.nsHighCardPoints)

                val slFitLenTv = getTextView(holder, R.id.tv_fit_len)
                slFitLenTv.text = when {
                    board.scoringLineFitLength == 0 -> getString(R.string.none)
                    else -> board.scoringLineFitLength.toString()
                }
                val slFitSuitIv = holder.itemView.findViewById<ImageView>(R.id.iv_fit_suit)
                if (board.scoringLineFitSuit != Suit.none) {
                    slFitSuitIv.setImageResource(suitImageResId(board.scoringLineFitSuit))
                    slFitSuitIv.visibility = View.VISIBLE
                } else {
                    doAssert(board.scoringLineFitLength == 0)
                    slFitSuitIv.visibility = View.GONE
                }
            }
            bindBoardContractViewHolder(holder, board, getString(R.string.made),
                    brdActvity.boardDisplayScore(board), brdActvity.boardIsVulnerable(board))

            onBindViewHolderCommon(holder, board)
        }
    }
}
