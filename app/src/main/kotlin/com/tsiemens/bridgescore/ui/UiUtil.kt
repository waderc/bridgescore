package com.tsiemens.bridgescore.ui

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.support.v4.content.ContextCompat
import android.widget.RadioGroup
import android.widget.TextView
import com.tsiemens.bridgescore.R
import com.tsiemens.bridgescore.Seat
import com.tsiemens.bridgescore.Suit
import java.text.SimpleDateFormat
import java.util.*

class SeatSelectionHelper(val group: RadioGroup,
                          val northId: Int,
                          val eastId: Int,
                          val southId: Int,
                          val westId: Int ) {

    fun getSelectedSeat(): Seat {
        when(group.checkedRadioButtonId) {
            -1 -> return Seat.none
            northId -> return Seat.north
            eastId -> return Seat.east
            southId -> return Seat.south
            westId -> return Seat.west
            else -> throw IllegalStateException("Unrecognized Id " + group.checkedRadioButtonId)
        }
    }

    fun setSelectedSeat(seat: Seat) {
        val rbId = when(seat) {
            Seat.none -> null
            Seat.north -> northId
            Seat.east -> eastId
            Seat.south -> southId
            Seat.west -> westId
        }

        if(rbId == null) {
            group.clearCheck()
        } else {
            group.check(rbId)
        }
    }
}

fun suitImageResId(suit: Suit): Int {
    when(suit) {
        Suit.noTrumps -> return R.drawable.ic_no_trumps
        Suit.spades -> return R.drawable.ic_spades_white
        Suit.hearts -> return R.drawable.ic_hearts
        Suit.diamonds -> return R.drawable.ic_diamonds
        Suit.clubs -> return R.drawable.ic_clubs_white
        else -> throw IllegalArgumentException("Invalid suit")
    }
}

fun fmtString(context: Context, strResId: Int, vararg args: Any?): String {
    return context.getString(strResId).format(*args)
}

fun sessionDateString(ctx: Context, timestamp: Long): String {
    val cal = Calendar.getInstance()
    cal.timeInMillis = timestamp
    val tz = cal.timeZone

    val dateFmt = SimpleDateFormat.getDateInstance()
    dateFmt.timeZone = tz

    val timeOfDayRes: Int?
    val hour = cal.get(Calendar.HOUR_OF_DAY)
    if(hour <= 11) {
        timeOfDayRes = R.string.time_morning
    } else if(hour <= 17) {
        timeOfDayRes = R.string.time_afternoon
    } else {
        timeOfDayRes = R.string.time_evening
    }

    return ctx.getString(timeOfDayRes) + ", " + dateFmt.format(cal.time)
}

fun prettyRoundedFloat(n: Float): String {
    if((n * 10).toLong() == n.toLong() * 10) {
        return String.format("%d", n.toInt())
    } else {
        return String.format("%.1f", n)
    }
}

fun getPointsDisplayColor(ctx: Context, pts: Int): Int {
    return ContextCompat.getColor(ctx, when {
        pts > 0 -> R.color.posGreen
        pts == 0 -> R.color.colorTextDisabled
        else -> R.color.negRed
    })
}

fun setBoardPointsText(tv: TextView, score: Int, fmt: String = "%s") {
    tv.text = String.format(fmt, String.format("%s%d", if(score > 0) "+" else "", score))
    tv.setTextColor(getPointsDisplayColor(tv.context, score))
}

fun setDirectionText(tv: TextView, northSouth: Boolean) {
    tv.text =  tv.context.getString(
            if(northSouth) R.string.north_south else R.string.east_west)
    val color = ContextCompat.getColor(
            tv.context,
            if(northSouth) R.color.colorNS else R.color.colorEW)
    tv.setTextColor(color)
}

fun dpToPx(ctx: Context, dp: Int): Int {
    val displayMetrics = ctx.getResources().getDisplayMetrics();
    return (dp* displayMetrics.density).toInt()
}

fun showThemedProgressDialog(ctx: Context, title: String?, msg: String?,
                             indeterminate: Boolean): ProgressDialog {
    //val dialog = ProgressDialog(ctx, android.R.style.Theme_Material_Dialog)
    // This doesn't seem to work on older APIs, so continue to use deprecated theme for convenience
    val dialog = ProgressDialog(ctx, AlertDialog.THEME_DEVICE_DEFAULT_DARK)
    if (title != null) {
        dialog.setTitle(title)
    }
    if (msg != null) {
        dialog.setMessage(msg)
    }
    dialog.isIndeterminate = indeterminate
    dialog.show()
    return dialog
}
