package com.tsiemens.bridgescore.ui

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.FragmentTransaction
import android.view.*
import android.widget.*
import com.tsiemens.bridgescore.R
import com.tsiemens.bridgescore.TRACE_ENABLED
import com.tsiemens.bridgescore.doAssert
import com.tsiemens.bridgescore.orm.BridgeBoardSet
import com.tsiemens.bridgescore.orm.BridgeSession
import com.tsiemens.bridgescore.orm.Orm
import com.tsiemens.bridgescore.orm.SessionSyncMode
import com.tsiemens.bridgescore.preference.PREF_DEFAULT_SYNC_SERVER
import com.tsiemens.bridgescore.preference.PREF_DEFAULT_SYNC_SERVER_PASSWORD
import com.tsiemens.bridgescore.preference.PREF_DEFAULT_TEAM_NAME

class SyncConfigActivity : AppCompatActivity() {

    companion object {
        val BRIDGE_SESSION_ID = "session_id"
    }

    var session: BridgeSession? = null
    var myBoardSet: BridgeBoardSet? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sync_config)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val sessionId = intent.getLongExtra("session_id", -1)
        doAssert(sessionId != -1L)
        session = Orm.findById(BridgeSession::class.java, sessionId)
        myBoardSet = session!!.myBoardSet()

        val hostFrag = HostSelectionFragment()
        hostFrag.activity = this
        supportFragmentManager.beginTransaction().add(R.id.frag_container, hostFrag)
                .addToBackStack(null)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit()

        val backButton: Button = findViewById(R.id.btn_back)
        backButton.setOnClickListener { view -> supportFragmentManager.popBackStack() }

        val nextButton: Button = findViewById(R.id.btn_next)
        nextButton.setOnClickListener { view -> onNextButtonPressed() }

        supportFragmentManager.addOnBackStackChangedListener {
            if(supportFragmentManager.backStackEntryCount == 0) {
                finish()
            } else {
                invalidateOptionsMenu()
            }
        }
    }

    fun getCurrentFragment(): SyncConfigFragment? {
        val frags = supportFragmentManager.fragments
        if (frags.size == 0) {
            return null
        } else {
            return frags[frags.size - 1] as SyncConfigFragment
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater;
        inflater.inflate(R.menu.sync_config_menu, menu);
        menu!!.findItem(R.id.debug).isVisible = TRACE_ENABLED
        return true;
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        val syncItem = menu!!.findItem(R.id.sync)
        syncItem.isVisible = getCurrentFragment() is SessionSelectionFragment
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId) {
            android.R.id.home -> finish()
            R.id.sync -> {
                val frag = getCurrentFragment()!!
                if(frag is SessionSelectionFragment) {
                    frag.updateSessionList()
                }
            }
            R.id.debug -> startActivity(Intent(this, DebugActivity::class.java))
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    fun onNextButtonPressed() {
        val nextFragCls = getCurrentFragment()!!.onNextPressed()
        if(nextFragCls != null) {
            val nextFrag = nextFragCls.getConstructor().newInstance()
            nextFrag.activity = this@SyncConfigActivity
            supportFragmentManager.beginTransaction().replace(R.id.frag_container, nextFrag)
                    .addToBackStack(null)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
        }
    }

    fun notifyDoneConfig() {
        Orm.doInTransaction {
            Orm.save(session!!)
            Orm.save(myBoardSet!!)
        }
        finish()
    }

    class HostSelectionFragment : SyncConfigFragment() {
        var hostText: EditText? = null
        var passwordText: EditText? = null
        var modeSwitch: Switch? = null
        var defaultCheck: CheckBox? = null

        var teamText: EditText? = null
        var teamDefaultCheck: CheckBox? = null

        override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                                  savedInstanceState: Bundle?): View? {
            val v = inflater!!.inflate(R.layout.fragment_sync_config_host, container, false)
            val prefs = PreferenceManager.getDefaultSharedPreferences(activity)
            val session = activity!!.session!!
            hostText = v.findViewById(R.id.et_sync_domain)
            hostText!!.text.clear()
            hostText!!.text.append(if(session.syncServer != null) session.syncServer
                    else prefs.getString(PREF_DEFAULT_SYNC_SERVER, ""))

            passwordText = v.findViewById(R.id.et_sync_domain_password)
            passwordText!!.text.clear()
            passwordText!!.text.append(
                    if(session.syncServerPassword != null) session.syncServerPassword
                    else prefs.getString(PREF_DEFAULT_SYNC_SERVER_PASSWORD, ""))

            modeSwitch = v.findViewById(R.id.switch_view_only)
            modeSwitch!!.isChecked = activity!!.session!!.syncMode != SessionSyncMode.pullOnly

            defaultCheck = v.findViewById(R.id.checkbox_sync_defaults)

            val myTeam = activity!!.myBoardSet!!.teamName
            teamText = v.findViewById(R.id.et_team_name)
            teamText!!.text.clear()
            teamText!!.text.append(if(!myTeam.isEmpty()) myTeam
                    else prefs.getString(PREF_DEFAULT_TEAM_NAME, ""))

            teamDefaultCheck = v.findViewById(R.id.checkbox_team_defaults)
            return v
        }

        override fun onNextPressed(): Class<out SyncConfigFragment>? {
            val host = hostText!!.text.toString()
            val team = teamText!!.text.toString()
            if(host.isEmpty()) {
                Toast.makeText(activity, R.string.sync_host_required, Toast.LENGTH_SHORT).show()
                return null
            }
            if(team.isEmpty()) {
                Toast.makeText(activity, R.string.team_name_required, Toast.LENGTH_SHORT).show()
                return null
            }

            activity!!.session!!.syncServer = host
            val password = passwordText!!.text.toString()
            activity!!.session!!.syncServerPassword = if(password.isEmpty()) null else password

            activity!!.session!!.syncMode = if(modeSwitch!!.isChecked) SessionSyncMode.pushPull
                else SessionSyncMode.pullOnly

            activity!!.myBoardSet!!.teamName = team

            if(defaultCheck!!.isChecked) {
                val prefs = PreferenceManager.getDefaultSharedPreferences(activity)
                prefs.edit()
                        .putString(PREF_DEFAULT_SYNC_SERVER, host)
                        .putString(PREF_DEFAULT_SYNC_SERVER_PASSWORD, password)
                        .apply()
            }

            if(teamDefaultCheck!!.isChecked) {
                val prefs = PreferenceManager.getDefaultSharedPreferences(activity)
                prefs.edit()
                        .putString(PREF_DEFAULT_TEAM_NAME, team)
                        .apply()
            }

            return SessionSelectionFragment::class.java
        }

        override fun getTitle() = R.string.sync_frag_title_config
        override fun prevButtonEnabled() = false
    }
}
