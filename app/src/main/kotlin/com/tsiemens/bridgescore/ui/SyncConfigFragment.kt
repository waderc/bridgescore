package com.tsiemens.bridgescore.ui

import android.support.v4.app.Fragment
import android.view.View
import android.widget.Button
import com.tsiemens.bridgescore.R

abstract class SyncConfigFragment() : Fragment() {
    var activity: SyncConfigActivity? = null

    override fun onStart() {
        super.onStart()
        activity!!.supportActionBar!!.setTitle(getTitle())
        activity!!.findViewById<View>(R.id.btn_back)!!.visibility =
                if(prevButtonEnabled()) View.VISIBLE else View.GONE
        (activity!!.findViewById<Button>(R.id.btn_next)!!).text =
                getString(nextButtonTextId())

        activity!!.invalidateOptionsMenu()
    }

    abstract fun onNextPressed(): Class<out SyncConfigFragment>?
    abstract fun getTitle(): Int

    open fun prevButtonEnabled() = true
    open fun nextButtonTextId() = R.string.next
}