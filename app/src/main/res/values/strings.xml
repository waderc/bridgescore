<resources>
    <string name="app_name">Bridgion</string>
    <string name="copyright">\u00A9 2016 Trevor Siemens</string>
    <string name="title_activity_board_list">Session Boards</string>
    <string name="title_activity_board_editor">Edit Board</string>
    <string name="title_activity_duplicate_comparison">Compare Duplicate Scores</string>

    <string name="empty_string"/>
    <string name="dash">-</string>
    <string name="undo">UNDO</string>

    <string name="vul_both">Both</string>
    <string name="vul_ns">N/S</string>
    <string name="vul_ew">E/W</string>
    <string name="none">None</string>

    <string name="north_abv">N</string>
    <string name="east_abv">E</string>
    <string name="south_abv">S</string>
    <string name="west_abv">W</string>

    <string name="north_south">N/S</string>
    <string name="east_west">E/W</string>

    <string name="time_morning">Morning</string>
    <string name="time_afternoon">Afternoon</string>
    <string name="time_evening">Evening</string>

    <string name="save">Save</string>
    <string name="edit">Edit</string>
    <string name="neww">New</string>
    <string name="done">Done</string>
    <string name="next">Next</string>
    <string name="compare">Compare Scores</string>

    <string name="honors_none">None</string>
    <string name="honors_all">All</string>
    <string name="honors_part">Partial</string>
    <string name="honors_all_def">All (defence)</string>
    <string name="honors_part_def">Partial (defence)</string>

    <!-- Main -->
    <string name="about_title_fmt">About %s</string>
    <string name="about_version_copy_fmt">v%1$s (build %2$d)\n\n%3$s</string>

    <string name="session_board_count_fmt">%d BOARD</string>
    <string name="session_board_count_plural_fmt">%d BOARDS</string>
    <string name="session_type_char_duplicate">D</string>
    <string name="session_type_char_teams">T</string>
    <string name="session_type_char_rubber">R</string>
    <string name="session_type_char_par_score">P</string>
    <string name="session_list_deleted_sessions_fmt">Deleted %d sessions</string>

    <string name="settings">Settings</string>
    <string name="about">About</string>

    <string name="session_edit_title_fmt">%s Bridge Session</string>
    <string name="teams">Teams</string>
    <string name="duplicate">Duplicate</string>
    <string name="rubber">Rubber</string>
    <string name="par_score">Par Score</string>
    <string name="session_name_hint">Session name eg. "Tourney"</string>
    <string name="boards_per_round">Boards Per Round</string>
    <string name="boards_per_round_hint">eg. 2</string>
    <string name="my_seat">My Seat</string>
    <string name="update_my_seat_on_all_boards">Update my seat on all boards</string>

    <string name="session_editor_no_bpr">Enter boards per round</string>
    <string name="session_editor_bpr_error">Boards per round is invalid</string>
    <string name="editor_no_my_seat">Select your seat</string>

    <!-- Board List -->
    <string name="board_list_all_rounds">All Rounds</string>
    <string name="board_list_round_fmt">Round %d</string>
    <string name="board_list_deleted_boards_fmt">Deleted %d boards</string>

    <string name="declarer_fmt">Decl\'r: %s</string>
    <string name="north">North</string>
    <string name="east">East</string>
    <string name="south">South</string>
    <string name="west">West</string>

    <string name="declarer">Declarer</string>
    <string name="dummy">Dummy</string>
    <string name="defence">Defence</string>

    <string name="vuln_flag">v.</string>
    <string name="made">Made</string>

    <string name="hcp_fmt">%d HCP</string>

    <string name="we_imps_total_fmt">WE: %d</string>
    <string name="they_imps_total_fmt">THEY: %d</string>

    <string name="board_round_change_hint_fmt">Now showing round %d</string>
    <string name="board_round_change_hint_action">Show all rounds</string>

    <string name="rubber_tab_boards">Boards</string>
    <string name="rubber_tab_scoreboard">Scoreboard</string>

    <string name="we_header">WE</string>
    <string name="they_header">THEY</string>

    <string name="contract_honors_fmt">w/ honors: %s</string>

    <!-- Board Editor -->
    <string name="board">Board</string>
    <string name="my_seat_colon_fmt">My Seat: %s</string>
    <string name="board_editor_vul_fmt">Vul: %s</string>
    <string name="board_editor_dealer_fmt">Dealer: %s</string>
    <string name="undoubled">Undoubled</string>
    <string name="doubled">Doubled</string>
    <string name="redoubled">Redoubled</string>
    <string name="made_caps">Made</string>
    <string name="down_caps">Down</string>
    <string name="scoring_line_header">Scoring Line:</string>
    <string name="ns_hcp_header">N/S HCP:</string>
    <string name="par_colon_fmt">Par: %s</string>
    <string name="scoring_line_fit_header">Scoring Line Fit</string>
    <string name="board_editor_board_no_used_fmt">Board %d already exists</string>
    <string name="board_editor_no_board_num">Enter the board number</string>
    <string name="board_editor_invalid_board_num">Board number is invalid</string>
    <string name="board_editor_no_decl_seat">Select the declarer\'s seat</string>
    <string name="board_editor_no_level">Select the contract level</string>
    <string name="board_editor_no_suit">Select the contract suit</string>
    <string name="board_editor_no_tricks">Enter the tricks made</string>
    <string name="board_editor_no_sl_hcp">Enter North/South\'s High Card Points</string>
    <string name="board_editor_invalid_sl_hcp">Invalid High Card Points</string>
    <string name="board_editor_no_sl_fit_len">Select the scoring line\'s longest fit length"</string>
    <string name="board_editor_no_sl_fit_suit">Select the scoring line\'s longest fit suit</string>
    <string name="board_editor_notes_hint">Notes</string>

    <string name="honors_header">Honors:</string>

    <string name="no_bid">No Bid</string>
    <string name="no_bid_dialog_msg">Set this board as having no bid made?</string>

    <!-- Duplicate comparisons -->
    <string name="total">Total</string>
    <string name="board_number_fmt">Board %d</string>

    <string name="publish_my_scores">Publish My Scores</string>
    <string name="sync">Sync</string>
    <string name="sync_settings">Sync Settings</string>
    <string name="unpublished_changes_snackbar">Your scores contain unpublished changes</string>
    <string name="publish">Publish</string>

    <string name="failed_to_get_scores_from_server">Failed to get scores from server</string>
    <string name="failed_to_publish_scores">Failed to publish scores to server</string>

    <string name="select_team_hint">Select/reset the other half of your team by long pressing</string>
    <string name="cannot_select_self_for_team">Cannot select yourself as a teammate</string>
    <string name="teammate_reset">Teammate Reset</string>

    <!-- Sync config -->
    <string name="sync_config_header">Set up your bridge session to sync scores with other Bridgion users</string>
    <string name="sync_domain_header">Server Details</string>
    <string name="sync_domain_hint">domain eg. score.mybridgeclub.com</string>
    <string name="sync_enable_publishing">Enable Board Score Publishing</string>
    <string name="sync_view_only_hint">Disable this if your partner is publishing your scores for you.</string>
    <string name="sync_team_header">Team Name</string>
    <string name="team_name_hint">eg. "Alice &amp; Bob"</string>
    <string name="set_as_defaults">Save these as defaults</string>
    <string name="team_set_as_default">Save as default team name</string>

    <string name="sync_frag_title_config">Score Sync Setup</string>
    <string name="sync_frag_title_session">Session Selection</string>

    <string name="password_optional">password (optional)</string>
    <string name="password">password</string>
    <string name="sync_host_required">Domain is required</string>
    <string name="team_name_required">Team name is required</string>

    <string name="new_remote_session_title_fmt">Create Bridge Session on %s</string>
    <string name="session_name_required">Session name is required</string>
    <string name="session_sync_session_required">Select a session</string>
    <string name="loading_remote_sessions_dialog_text_fmt">Getting sessions from %s…</string>
    <string name="failed_to_load_remote_sessions_fmt">Failed to get sessions from %s</string>
    <string name="create_remote_session_dialog_text">Creating session…</string>
    <string name="failed_to_create_remote_session">Failed to create new session</string>

    <string name="delete_confirm_remote_session_dialog_title">Deleting Session</string>
    <string name="delete_confirm_remote_session_dialog_text_fmt">Delete configuration for the %1$s %2$s duplicate session?</string>
    <string name="delete_remote_session_dialog_text">Deleting session…</string>
    <string name="failed_to_delete_remote_session">Failed to delete session</string>

    <!-- Preferences -->
    <string name="pref_default_session_name">Default Session Name</string>
    <string name="pref_default_team_name">Default Team Name</string>
    <string name="pref_default_boards_per_session">Default Boards Per Session</string>
    <string name="pref_default_sync_server">Default League Server Domain</string>
    <string name="pref_default_sync_server_password">Default League Server Password</string>
    <string name="title_activity_settings">Settings</string>

    <string name="default_boards_value_warning">Default boards per session must be greater than 0</string>
    <string name="int_pref_value_warning">Value must be a number</string>

    <string name="debug_screen">Debug Screen</string>
    <string name="logcats">Logcats</string>

</resources>
