package com.tsiemens.bridgescore

import com.tsiemens.bridgescore.orm.*
import java.util.*

class OrmMockTable<T: OrmRecord> {
    val entries = HashMap<Long, T>()
    var nextId = 1L

    fun get(id: Long): T? = entries.get(id)

    fun save(entry: T): Long {
        if(entry.getid() == null) {
            entry.setid(nextId)
            nextId++
        }
        entries.put(entry.getid()!!, entry)
        return entry.getid()!!
    }

    fun remove(id: Long): T? {
        return entries.remove(id)
    }
}

class OrmMockDb {
    companion object {
        val boardTable = OrmMockTable<BridgeBoard>()
        val boardSetTable = OrmMockTable<BridgeBoardSet>()
        val sessTable = OrmMockTable<BridgeSession>()

        fun <T: OrmRecord> getTable(clazz: Class<T>): OrmMockTable<T> = when(clazz) {
            BridgeBoard::class.java -> OrmMockDb.boardTable as OrmMockTable<T>
            BridgeBoardSet::class.java -> OrmMockDb.boardSetTable as OrmMockTable<T>
            BridgeSession::class.java -> OrmMockDb.sessTable as OrmMockTable<T>
            else -> throw IllegalArgumentException()
        }
    }
}

class OrmMocksWrapper : OrmWrapper() {
    override fun <T: OrmRecord> findById(clazz: Class<T>, id: Long): T? {
        return OrmMockDb.getTable(clazz).get(id)
    }

    override fun <T: OrmRecord> find(clazz: Class<T>, query: String, vararg args: String): List<T> {
        return when (clazz) {
            BridgeBoard::class.java ->
                when (query) {
                    "board_set_id = ?" -> OrmMockDb.boardTable.entries.filter { v ->
                        v.value.boardSetId == args.component1().toLong() }.values.toList() as List<T>
                    else -> throw UnsupportedOperationException(query)
                }
            BridgeBoardSet::class.java ->
                    when (query) {
                        "session_id = ? AND publisher_id is NULL" -> OrmMockDb.boardSetTable.entries.filter {
                            v -> v.value.sessionId == args.component1().toLong() &&
                                v.value.isMine() }.values.toList() as List<T>
                        else -> throw UnsupportedOperationException(query)
                    }
            else -> throw UnsupportedOperationException(clazz.toString())
        }
    }

    override fun <T: OrmRecord> findAll(clazz: Class<T>): Iterator<T> {
        TODO()
    }

    override fun <T: OrmRecord> count(clazz: Class<T>, query: String, args: Array<String>): Long {
        TODO()
    }

    override fun save(obj: OrmRecord): Long {
        doAssert(obj.isValid())
        return when(obj.javaClass) {
            BridgeBoard::class.java -> OrmMockDb.boardTable.save(obj as BridgeBoard)
            BridgeBoardSet::class.java -> OrmMockDb.boardSetTable.save(obj as BridgeBoardSet)
            BridgeSession::class.java -> OrmMockDb.sessTable.save(obj as BridgeSession)
            else -> throw IllegalArgumentException()
        }
    }

    override fun delete(obj: OrmRecord): Boolean {
        val table =  when(obj.javaClass) {
            BridgeBoard::class.java -> OrmMockDb.boardTable
            BridgeBoardSet::class.java -> OrmMockDb.boardSetTable
            BridgeSession::class.java -> OrmMockDb.sessTable
            else -> throw IllegalArgumentException()
        }
        if(table.get(obj.getid()!!) == null) {
            return false
        }
        table.remove(obj.getid()!!)
        return true
    }

    override fun doInTransaction(callback: () -> Unit) {
        callback()
    }
}