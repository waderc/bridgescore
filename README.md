# BridgeScore

### Description
BridgeScore is an Android app which hopes to track scores for Duplicate Bridge and Rubber Bridge.

It is also compatible with [bridgescoreserver](https://bitbucket.org/tsiemens/bridgescoreserver),
which is used to sync scores between players to calculate Duplicate matchpoints.
Though it is not specified, for security reasons, the server should be hosted using HTTPS.
If necessary, an HTTP fallback may be provided for devices which are incompatible
with modern versions of TLS.

### Technical Details
This app is partly a personal experiment in using the new [Kotlin](https://kotlinlang.org/)
 language, which is meant to interop easily with Java, and offers features like a nicer
 syntax, built in null safety, lambdas, etc. As such, the Kotlin plugin for IntelliJ is required.

Database mapping is currently provided by [SugarORM](https://satyan.github.io/sugar/),
 which automatically handles database schema creation and query construction.

### Set Up
* Install Android Studio
* Install the Kotlin plugin for Android Studio
* Open this idea project from Android Studio
